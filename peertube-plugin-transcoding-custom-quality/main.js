async function register ({
  registerSetting,
  settingsManager,
  transcodingManager
}) {
  const defaultCRF = 23

  const store = {
    crf: await settingsManager.getSetting('crf') || defaultCRF
  }

  settingsManager.onSettingsChange(settings => {
    store.crf = settings['crf']
  })

  const builderVOD = (options) => {
    return {
      outputOptions: [
        `-r ${options.fps}`,
        `-crf ${store.crf}`
      ]
    }
  }

  const buildLive = (options) => {
    return {
      outputOptions: [
        `${buildStreamSuffix('-r:v', options.streamNum)} ${options.fps}`,
        `-crf ${store.crf}`
      ]
    }
  }

  registerSetting({
    name: 'crf',
    label: 'Quality',
    type: 'select',
    options: [
      { label: 'Low', value: 35 },
      { label: 'Medium', value: 32 },
      { label: 'Good', value: 29 },
      { label: 'Very good', value: 26 },
      { label: 'Excellent (Peertube default)', value: 23 },
      { label: 'Perfect', value: 20 },
      { label: 'Insane', value: 17 },
    ],
    descriptionHTML: '<p>Increasing quality will result in bigger video sizes.</p><p>NB : Peertube default encoding settings cap the bit rate of videos. Image quality decreases sensibly in scenes with a lot of moving details, like a tracking shot in a landscape. This plugin removes the cap, resulting in constant quality, high bit rates on such scenes. Depending on your videos, the "Excellent (Peertube default uncapped)" setting might yield much bigger size and higher quality than Peertube default.</p>',
    private: true,
    default: defaultCRF
  })

  const encoder = 'libx264'
  const profileName = 'custom-quality'

  transcodingManager.addVODProfile(encoder, profileName, builderVOD)
  transcodingManager.addLiveProfile(encoder, profileName, buildLive)
}

async function unregister () {
  return
}

module.exports = {
  register,
  unregister
}


// ---------------------------------------------------------------------------

function buildStreamSuffix (base, streamNum) {
  if (streamNum !== undefined) {
    return `${base}:${streamNum}`
  }

  return base
}

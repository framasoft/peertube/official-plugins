const shared = require('./shared-player')

function register ({ registerHook, peertubeHelpers }) {
  registerHook({
    target: 'action:embed.player.loaded',
    handler: ({ player, videojs, video }) => shared.buildPlayer({ video, player, videojs, peertubeHelpers })
  })
}

export {
  register
}

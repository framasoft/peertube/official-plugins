const shared = require('./shared-player')

function register ({ registerHook, peertubeHelpers }) {
  registerHook({
    target: 'action:video-watch.player.loaded',
    handler: ({ player, videojs, video }) => shared.buildPlayer({ video, player, videojs, peertubeHelpers })
  })
}

export {
  register
}
